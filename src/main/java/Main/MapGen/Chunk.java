package Main.MapGen;

import Main.MapGen.CellularAutomata.CellularAutomata;
import Main.MapGen.PerlinNoise.PerlinNoise;
import Main.Sprites.Block;
import org.jsfml.graphics.RenderWindow;
import org.jsfml.graphics.Texture;
import org.jsfml.system.Vector2f;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Chunk
{
    private Block[][] screenBlockArray;


    private int chunkSizeBlocks;
    private int chunkSizePixels;
    private Vector2f chunkPosition;

    private Path wallTexturePath = Paths.get("D:\\University\\Year2\\TerrariaGame\\Assets\\WallTexture.png");
    private Path emptyTexturePath = Paths.get("D:\\University\\Year2\\TerrariaGame\\Assets\\WaterTexture.png");
    private Path diamondTexturePath = Paths.get("D:\\University\\Year2\\TerrariaGame\\Assets\\DiamondTexture.png");
    private Path grassTexturePath = Paths.get("D:\\University\\Year2\\TerrariaGame\\Assets\\GrassTexture.png");
    private Texture wallTexture = new Texture();
    private Texture emptyTexture = new Texture();
    private Texture diamondTexture= new Texture();
    private Texture grassTextue = new Texture();

    //private Generator chunkDesign;





    public Chunk(int chunkSizeBlocks, int chunkSizePixels, Vector2f chunkPosition)
    {
        this.chunkSizeBlocks = chunkSizeBlocks;
        this.chunkSizePixels = chunkSizePixels;
        this.chunkPosition = chunkPosition;

        try
        {
            wallTexture.loadFromFile(wallTexturePath);
            emptyTexture.loadFromFile(emptyTexturePath);
            diamondTexture.loadFromFile(diamondTexturePath);
            grassTextue.loadFromFile(grassTexturePath);
        }
        catch(Exception E)
        {

        }

        screenBlockArray = createMap(generateChunkCellularAutomata());  //change this Line
    }

    public Generator generateChunkCellularAutomata()
    {
        return new CellularAutomata(chunkSizeBlocks,chunkSizePixels, chunkPosition);
    }

    public Generator generateChunkPerlinNoise()
    {
        return new PerlinNoise(chunkSizeBlocks,chunkSizePixels, chunkPosition);
    }

    public Block[][] createMap(Generator chunkDesign)
    {
        Vector2f[][] blockPosition = generateLocations();
        Block[][] blockMap = new Block[chunkSizeBlocks][chunkSizeBlocks];
        int[][] mapBluePrint = chunkDesign.getChunkBinaryMapping();
        for (int a = 0; a < chunkSizeBlocks; a++)
        {
            for (int b = 0; b < chunkSizeBlocks; b++)
            {
                if (mapBluePrint[a][b] == 1)
                {

                    blockMap[a][b] = new Block(0,emptyTexture);


                }
                else
                {
                    blockMap[a][b] = new Block(0,wallTexture);

                }
                blockMap[a][b].setPosition(new Vector2f((chunkPosition.x * chunkSizePixels) + blockPosition[a][b].x, (chunkPosition.y * chunkSizePixels) + blockPosition[a][b].y));
                blockMap[a][b].scale(1f,1f);
            }
        }

        return blockMap;
    }

    public void drawScreen(RenderWindow window)
    {
        for (int a = 0; a < chunkSizeBlocks; a++)
        {
            for (int b = 0; b < chunkSizeBlocks; b++)
            {
                window.draw(screenBlockArray[a][b]);


            }
        }

    }

    public Vector2f[][] generateLocations()
    {
        Vector2f[][] locations = new Vector2f[chunkSizeBlocks][chunkSizeBlocks];
        float step = (chunkSizePixels / chunkSizeBlocks);


        for (int a = 0; a < chunkSizeBlocks; a++)
        {
            for (int b = 0; b < chunkSizeBlocks; b++)
            {
                locations[a][b] = new Vector2f(a*step, b*step);
            }
        }
        return locations;
    }


    public boolean equals(Vector2f o)
    {
        if (o.x == getChunkPosition().x && o.y == getChunkPosition().y)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public Vector2f getChunkPosition()
    {
        return chunkPosition;
    }


}
